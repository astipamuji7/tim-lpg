# Nama  : Asti Dwi Sripamuji
# NIM   : 19102006
# Kelas : S1IF-07-SC1

import pandas as pd
import time

# fungsi pengurutan "Bubble Sort"
def bubbleSort(array):
    
  # perulangan melalui setiap elemen array
  for i in range(len(array)):
        
    # memantau pertukaran
    swapped = False
    
    # perulangan untuk membandingkan elemen array
    for j in range(0, len(array) - i - 1):

      # membandingkan dua elemen yang berdekatan
      # ubah > menjadi < untuk mengurutkan dalam urutan menurun
      if array[j] > array[j + 1]:

        # swapping terjadi jika elemen
        # tidak dalam urutan yang dimaksudkan
        temp = array[j]
        array[j] = array[j+1]
        array[j+1] = temp

        swapped = True
          
    # tidak ada swapping berarti array sudah diurutkan
    # jadi tidak perlu perbandingan lebih lanjut
    if not swapped:
      break

# fungsi pengujian
def uji (banyak):

  # menginisialisasi variabel start
  # sebagai waktu mulai program
  start = time.time()
  
  # membaca data csv
  data = pd.read_csv("data/" + str(banyak) + "_data.csv")

  # menginputkan data kedalam list
  array = []
  for i in range(banyak):
      array.append(data['0'][i])

  # mengeksekusi fungsi pengurutan
  bubbleSort(array)

  # mencetak hasil urutan
  # print("Hasil terurut dari " + str(banyak) + " data:")
  # print(array)
  
  # menginisialisasi variabel end
  # sebagai waktu berakhirnya program
  end = time.time()

  # menunjukan waktu eksekusi program
  print("Waktu eksekusi program :", end-start)

# list nama file data
files = [10, 100, 1000, 10000, 25000, 50000, 75000, 100000]

# mengeksekusi fungsi pengujian
for _ in files:
  uji(_)


