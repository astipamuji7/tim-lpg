# Nama  : Asti Dwi Sripamuji
# NIM   : 19102006
# Kelas : S1IF-07-SC1

import pandas as pd
import time

# fungsi pengurutan "Merge Sort"
def mergeSort(array):
    if len(array) > 1:

        #  membagi array menjadi dua sub-array
        r = len(array)//2
        L = array[:r]
        M = array[r:]

        # mengurutkan dua bagian
        mergeSort(L)
        mergeSort(M)

        i = j = k = 0

        # pada saat mencapai salah satu ujung L atau M, 
        # dipilih yang lebih besar di antara
        # elemen L dan M kemudian diletakkan di posisi yang benar
        while i < len(L) and j < len(M):
            if L[i] < M[j]:
                array[k] = L[i]
                i += 1
            else:
                array[k] = M[j]
                j += 1
            k += 1

        # ketika elemen di L atau M habis,
        # diambil elemen yang tersisa dan dimasukkan
        while i < len(L):
            array[k] = L[i]
            i += 1
            k += 1

        while j < len(M):
            array[k] = M[j]
            j += 1
            k += 1

# fungsi pencetak hasil terurut
def printList(array):
    for i in range(len(array)):
        print(array[i], end=" ")
    print()

# fungsi pengujian
def uji (banyak):

  # menginisialisasi variabel start
  # sebagai waktu mulai program
  start = time.time()
  
  # membaca data csv
  data = pd.read_csv("data/" + str(banyak) + "_data.csv")

  # menginputkan data kedalam list
  array = []
  for i in range(banyak):
      array.append(data['0'][i])

  # memanggil fungsi pengurutan
  mergeSort(array)

  # mencetak hasil urutan
  # print("Hasil terurut dari " + str(banyak) + " data:")
  # printList(array)
  
  # menginisialisasi variabel end
  # sebagai waktu berakhirnya program
  end = time.time()

  # menunjukan waktu eksekusi program
  print("Waktu eksekusi program :", end-start)

# list nama file data
files = [10, 100, 1000, 10000, 25000, 50000, 75000, 100000]

# mengeksekusi fungsi pengujian
for _ in files:
  uji(_)
